<%-- 
    Document   : addEmployeForm
    Created on : 7 nov. 2019, 13:44:53
    Author     : 33760
--%>

<%@page import="web.lsi.Utilisateur"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Ajouter un Employé</title>
        <link type="text/css" rel="stylesheet" href="css/style.css">
        <link type="text/css" rel="stylesheet" href="css/add-employe-style.css">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    </head>
    <body>
        <div id="wrapper">
            <div id="header">
                <h2>Employés JAKARTA</h2>
            </div>
        </div>
        
        <div id="container">
            <h3>Ajouter Employé</h3>
            
            <form action="EmployeControllerServlet" method="GET">
                <div id="bienvenue" align="right">
                    <%
                        Utilisateur user = (Utilisateur) session.getAttribute("userBean");
                    %>
                    Bonjour <jsp:getProperty name="userBean" property="login" /> ! Votre session est active.
                    <button type="submit" name="action" value="logout" formaction="Controller">
                        <img src="image/logout.png" alt="image de deconnexion" height="30" width="30"/>
                    </button>

                 </div>
                
                <input type="hidden" name="command" value="ADD"/>
                
                        <div class="form-group row">
                                <label for="nom" class="col-md-1 col-form-label">Nom</label>
                                <div class="col-md-5">
                                    <input id="nom" type="text" class="form-control" name="nom" value="${theEmploye.nom}"/>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="prenom" class="col-md-1 col-form-label">Prénom</label>
                                <div class="col-md-5">
                                    <input id="prenom" type="text" class="form-control" name="prenom" value="${theEmploye.prenom}"/>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="teldom" class="col-md-1 col-form-label">Tél dom</label>
                                <div class="col-md-5">
                                    <input id="teldom" type="text" class="form-control" name="telDom" value="${theEmploye.telDom}" />
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="telpor" class="col-md-1 col-form-label">Tél mob</label>
                                <div class="col-md-5">
                                    <input id="telpor" type="text" class="form-control" name="telPor" value="${theEmploye.telPort}" />
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="telpro" class="col-md-1 col-form-label">Tél Pro</label>
                                <div class="col-md-5">
                                    <input id="telpro" type="text" class="form-control" name="telPro" value="${theEmploye.telPro}"/>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="adresse" class="col-md-1 col-form-label">Adresse</label>
                                <div class="col-md-5">
                                    <input id="adresse" type="text" class="form-control" name="adresse" value="${theEmploye.adresse}"/>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="codepos" class="col-md-1 col-form-label">Code postal</label>
                                <div class="col-md-5">
                                    <input id="codepos" type="text" class="form-control" name="codePostal" value="${theEmploye.codePostal}"/>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="ville" class="col-md-1 col-form-label">Ville</label>
                                <div class="col-md-5">
                                    <input id="ville" type="text" class="form-control" name="ville" value="${theEmploye.ville}"/>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="email" class="col-md-1 col-form-label">Adresse e-mail</label>
                                <div class="col-md-5">
                                    <input id="email" type="text" class="form-control" name="email" value="${theEmploye.email}"/>
                                </div>
                            </div>
                                <button type="submit" name="action" value="add" formaction="Controller"
                                        class="employe-button btn btn-primary" id="add">
                                    Valider
                                </button>
                                <button type="submit" value="list" name="action" formaction="Controller"
                                    class="emplye-buttoon btn btn-light" id="datails">
                                    Voir liste
                                </button>
            </form>
            <c:if test="${!empty errAdd}">
                <p style="text-align: center; color: red;">
                    ${errAdd}
                </p>
            </c:if>
        </div>
    </body>
</html>


<%-- 
    Document   : list-employe
    Created on : Oct 23, 2019, 11:46:24 PM
    Author     : EKEUMOU, pierre
--%>

<%@page import="web.lsi.Utilisateur"%>
<%@page import="java.util.List"%>
<%@page import="web.lsi.model.EmployeBean"%>
<%-- We are going to start using JSTL, so instead of page we are going to use taglib --%>
<%-- @page import="java.util.*, com.jvprj.web.jdbc.*" contentType="text/html" pageEncoding="UTF-8" --%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Application de Gestion d'Employes</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    </head>
    
    <%-- No need doing all this because most of the work will be done for us by JSTL --%>
    <%--
        // 1. Get the employes from the request object (sent by servlet)
        List<Employe> theEmployes = (List<Employe>) request.getAttribute("EMPLOYE_LIST");
    --%>
    <body>
        <form action="Controller">
         <div id="bienvenue" align="right">
            <%
                Utilisateur user = (Utilisateur) session.getAttribute("userBean");
            %>
            Bonjour <jsp:getProperty name="userBean" property="login" /> ! Votre session est active.
            <button type="submit" name="action" value="logout" formaction="Controller">
                <img src="image/logout.png" alt="image de deconnexion" height="30" width="30"/>
            </button>

        </div>
       <%
            List<EmployeBean> tempEmploye;
            tempEmploye = (List<EmployeBean>) request.getAttribute("EMPLOYE_LIST");
            if(tempEmploye.isEmpty()){
        %>
                <p align="center"> <font color="0000FF"><c:out value="${NO_EMPLOYE_IN_BASE}"/> </font> </p>
            
                
        <%
            }else{
        %>
            <div id="wrapper">
                <div id="header">
                    <h2>Employes JAKARTA</h2>
                </div>
            </div>

            <div id="container">
                <div id="content">
                    <table class="table">
                        <tr>
                            <th>S�l</th>
                            <th>Nom</th>
                            <th>Pr�nom</th>
                            <th>Tel Domicile</th>
                            <th>Tel Portable</th>
                            <th>Tel Pro</th>
                            <th>Adresse</th>
                            <th>Code Postal</th>
                            <th>Ville</th>
                            <th>Email</th>
                        </tr>

                        <%-- Replacing our Scriplet with JSTL code --%>
                        <%-- <% for (Employe tempEmploye : theEmployes) { %>
                        <% } %>--%>                

                        <%--
                            - EMPLOYE_LIST : Same attribute that was set by the servlet
                            - So the JSTL tag will actually go in the request object and get the attribute calle EMPLOYE_LIST
                              and make it available
                            - So much of the work will be done behind the scenes for us by JSTL
                        --%>
                        <c:forEach var="tempEmploye" items="${EMPLOYE_LIST}">

                            <tr id="myEmployees">
                                <%-- Replacing the JSP scriplet expression with the JSP expression language --%>
                                <td> <input type="radio" name="selectedEmploye" value="${tempEmploye.id}"/> </td>
                                <td> ${tempEmploye.nom} </td>
                                <td> ${tempEmploye.prenom} </td>
                                <td> ${tempEmploye.telDom} </td>
                                <td> ${tempEmploye.telPort} </td>
                                <td> ${tempEmploye.telPro} </td>
                                <td> ${tempEmploye.adresse} </td>
                                <td> ${tempEmploye.codePostal} </td>
                                <td> ${tempEmploye.ville} </td>
                                <td> ${tempEmploye.email} </td>
                            </tr>
                        </c:forEach>
                    </table>
                            <!--<input type="button" value="delete" name="action"
                                   <%if(user.getLogin().equals("empl")){out.print("disabled");}%>
                                   window.onclick
                                   onclick="if (!(confirm('Voulez vous vraiment supprimer cette employe ?'))) return false;"
                                        class="employe-button" id="delete"
                            />
                            <input type="button" value="details" name="action"
                                   class="employe-button" id="details"
                            />
                            <input type="button" value="add" name="action"
                                   <%if(user.getLogin().equals("empl")){out.print("disabled");}%>
                                   class="employe-button" id="add"
                            />-->
                            <button type="submit" name="action" value="delete" formaction="Controller"
                                    <%if(user.getLogin().equals("empl")){out.print("disabled");}%>
                                        class="employe-button btn btn-primary" id="delete">
                                delete
                            </button>
                            <button type="submit" value="details" name="action" formaction="Controller"
                                    class="emplye-buttoon btn btn-primary" id="datails">
                                details
                            </button>
                            <button type="submit" name="action" value="add-panel" formaction="Controller"
                                    <%if(user.getLogin().equals("empl")){out.print("disabled");}%>
                                    class="employe-button btn btn-light" id="add">
                                add
                            </button>
                            
              
                </div>
            </div>
            <c:if test="${!empty NO_EMPLOYEE_SELECTED}">
                <div class="alert alert-danger" style="margin-top: 2px;">
                    ${NO_EMPLOYEE_SELECTED}
                </div>
            </c:if>
            <c:if test="${!empty NO_EMPLOYE_IN_BASE}">
                <div class="alert alert-danger" style="margin-top: 2px;">
                    ${NO_EMPLOYE_IN_BASE}
                </div>
            </c:if>
            <c:if test="${!empty deleteSuccess}">
                <div class="alert alert-success" style="margin-top: 2px;">
                    ${deleteSuccess}
                </div>
            </c:if>
            <c:if test="${!empty addSuccess}">
                <div class="alert alert-success" style="margin-top: 2px;">
                    ${addSuccess}
                </div>
            </c:if>


        <%
            }
        %>
    </form>
    </body>
</html>

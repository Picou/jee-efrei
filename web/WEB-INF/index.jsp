<%-- 
    Document   : index
    Created on : 19 sept. 2019, 10:37:20
    Author     : JAA
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <title>Index - LSI - M1</title>
    </head>
    <body>
<%--<%
    String messErreur = (String)request.getAttribute("errKey");
    if (messErreur != null){
        out.print(messErreur);
    }
    %>--%>

    <p style="text-align: center; color: red;">
        <c:if test="${!empty errKey}">
            ${errKey}
        </c:if>
    </p>
    
    <%--${!empty errKey ? errKey : ""}--%>

        <form method="post" class="col-4 offset-4" name="form1" action="Controller">
            <hr class="mb-1">
            <h2>Login</h2>
            <br>
            <input type="text" class="form-control" name="loginForm" placeholder="Login"/><br>
            <input type="password" class="form-control" name="mdpForm" placeholder="Mot de passe"/><br>
            <input type="submit" class="btn btn-primary" name="action" value="login"/>
        </form>
    </body>

</html>

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package web.lsi.utils;

/**
 *
 * @author pierre
 */
public class Constantes {
    public static final String URL = "jdbc:derby://localhost:1527/JEEPRJ";
    public static final String USER_BDD = "jee";
    public static final String MDP_BDD = "jee";
    
    // requetes
    public static final String REQ_TOUS_UTILISATEUR = "SELECT * from UTILISATEUR";
    public static final String REQ_TOUS_EMPL = "SELECT * from EMPLOYES";
    public static final String REQ_GET_EMPLOYE = "SELECT * from employes where id =?";
    public static final String REQ_ADD_EMPLOYE = "INSERT INTO EMPLOYES (NOM, PRENOM, TELDOM, TELPORT, TELPRO, ADRESSE, CODEPOSTAL, VILLE, EMAIL) values(?,?,?,?,?,?,?,?,?)";
    public static final String REQ_DELETE_EMPLOYE = "DELETE FROM employes where id=?";
    public static final String REQ_UPDATE_EMPLOYE = "update employes set nom=?, prenom=?, telDom=?, telPort=?, telPro=?, adresse=?, codePostal=?, ville=?, email=? where id=?";
    
    // messages
    public static final String NO_EMPLOYE_IN_BASE = "Nous devons recruter !";
    public static final String LOGIN_AND_OR_MDP_EMPTY = "Vous devez renseigner les deux champs.\n";
    public static final String LOGIN_AND_OR_MDP_WRONG = "Echec de la connexion ! Vérifiez votre login et/ou mot de passe et ressayez à nouveau.\n";
    public static final String ADDITION_FAILED = "Echec de l'ajout\n";
    public static final String DELETE_SUCCESSFUL = "Suppression réussie !";
    public static final String NO_EMPLOYEE_SELECTED ="Veuillez sélectionner l'employé(e) à supprimer";
    public static final String NO_EMPLOYEE_SELECTED_MODIF ="Veuillez sélectionner l'employé(e) à modifier";
    public static final String ADDITION_SUCCESS = "Succès de l'ajout";
}

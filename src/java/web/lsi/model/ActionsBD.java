package web.lsi.model;

import com.sun.corba.se.impl.orbutil.closure.Constant;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import web.lsi.Utilisateur;
import web.lsi.utils.Constantes;

/**
 *
 * @author JAA
 */
public class ActionsBD {

    Connection conn;
    Statement stmt;
    PreparedStatement pstmt;
    ResultSet rs;
    Utilisateur user;
    ArrayList<Utilisateur> listeUsers;
    ArrayList<EmployeBean> listeEmployes;
    int rowCount;

    public int getRowCount() {
        return rowCount;
    }

    public ActionsBD() {
        try {
            conn = DriverManager.getConnection(Constantes.URL, Constantes.USER_BDD, Constantes.MDP_BDD);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

    }

    public Statement getStatement() {
        try {
            stmt = conn.createStatement();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return stmt;
    }

    public ResultSet getResultSet(String req) {
        try {
            stmt = getStatement();
            
            rs = stmt.executeQuery(req);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return rs;
    }

    /**
     * Get all users from the DB
     * @return ArrayList of user
     */
    public ArrayList getUtilisateurs() {
        listeUsers = new ArrayList<>();
        try {
            rs = getResultSet(Constantes.REQ_TOUS_UTILISATEUR);
            
            while (rs.next()) {
                Utilisateur userBean = new Utilisateur();
                userBean.setLogin(rs.getString("LOGIN"));
                userBean.setMdp(rs.getString("PASSWORD"));

                listeUsers.add(userBean);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return listeUsers;
    }
    
    /**
     * Get all employees from the DB
     * @return ArrayList of employees
     */
    public ArrayList getEmployees() {
        listeEmployes = new ArrayList<>();
        try {
            rs = getResultSet(Constantes.REQ_TOUS_EMPL);
            
            while (rs.next()) {
                EmployeBean employeBean = new EmployeBean();
                employeBean.setId(rs.getInt("ID"));
                employeBean.setNom(rs.getString("NOM"));
                employeBean.setPrenom(rs.getString("PRENOM"));
                employeBean.setAdresse(rs.getString("ADRESSE"));
                employeBean.setVille(rs.getString("VILLE"));
                employeBean.setCodePostal(rs.getString("CODEPOSTAL"));
                employeBean.setTelDom(rs.getString("TELDOM"));
                employeBean.setTelPort(rs.getString("TELPORT"));
                employeBean.setTelPro(rs.getString("TELPRO"));
                employeBean.setEmail(rs.getString("EMAIL"));

                listeEmployes.add(employeBean);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return listeEmployes;
    }
    
    /**
     * Get a specific employee from the database
     * @param id of employee
     * @return Specific employee object
     */
    public EmployeBean getEmploye(int id){
        EmployeBean theEmployee = new EmployeBean();
        //int employeID = Integer.parseInt(id);
        
        try {
            pstmt = conn.prepareStatement(Constantes.REQ_GET_EMPLOYE);
            pstmt.setInt(1, id);
            rs = pstmt.executeQuery();
            
            while (rs.next()){
                theEmployee.setId(rs.getInt("ID"));
                theEmployee.setNom(rs.getString("NOM"));
                theEmployee.setPrenom(rs.getString("PRENOM"));
                theEmployee.setAdresse(rs.getString("ADRESSE"));
                theEmployee.setVille(rs.getString("VILLE"));
                theEmployee.setCodePostal(rs.getString("CODEPOSTAL"));
                theEmployee.setTelDom(rs.getString("TELDOM"));
                theEmployee.setTelPort(rs.getString("TELPORT"));
                theEmployee.setTelPro(rs.getString("TELPRO"));
                theEmployee.setEmail(rs.getString("EMAIL"));
            }
        } catch (SQLException ex) {
            Logger.getLogger(ActionsBD.class.getName()).log(Level.SEVERE, null, ex);
        }finally{
            try {
                pstmt.close();
            } catch (SQLException ex) {
                Logger.getLogger(ActionsBD.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return theEmployee;
    }

    /**
     * Check if user exists in the database
     * @param userInput
     * @return if the user exists
     */
    public boolean verifInfosConnexion(Utilisateur userInput) {
        boolean test = false;

        listeUsers = getUtilisateurs();

        for (Utilisateur userBase : listeUsers) {
            if (userBase.getLogin().equals(userInput.getLogin())
                    && userBase.getMdp().equals(userInput.getMdp())) {
                test = true;
            }
        }

        return test;
    }
    
    /**
     * Add new employee in the database
     * @param empl to add to database
     */
    public void addEmploye(EmployeBean empl) {
        
        try {
            pstmt = conn.prepareStatement(Constantes.REQ_ADD_EMPLOYE);
            
            // set param values
            pstmt.setString(1, empl.getNom());
            pstmt.setString(2, empl.getPrenom());
            pstmt.setString(3, empl.getTelDom());
            pstmt.setString(4, empl.getTelPort());
            pstmt.setString(5, empl.getTelPro());
            pstmt.setString(6, empl.getAdresse());
            pstmt.setString(7, empl.getCodePostal());
            pstmt.setString(8, empl.getVille());
            pstmt.setString(9, empl.getEmail());
            
            rowCount = pstmt.executeUpdate();
        } 
        catch (SQLException ex) {
            Logger.getLogger(ActionsBD.class.getName()).log(Level.SEVERE, null, ex);
        }
        finally{
            try {
                pstmt.close();
            } catch (SQLException ex) {
                Logger.getLogger(ActionsBD.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    /**
     * Delete an employee from the database
     * @param id of employee to delete
     */
    public void deleteEmploye(int id){
        try {
            
            // Create prepared statement
            pstmt = conn.prepareStatement(Constantes.REQ_DELETE_EMPLOYE);
            //Set param value
            pstmt.setInt(1, id);
            rowCount = pstmt.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(ActionsBD.class.getName()).log(Level.SEVERE, null, ex);
        }finally{
            try {
                pstmt.close();
            } catch (SQLException ex) {
                Logger.getLogger(ActionsBD.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
    }
    
    /**
     * update database fields of an employee
     * @param empl to update
     */
    public void update(EmployeBean empl){
        
        try {
            pstmt = conn.prepareStatement(Constantes.REQ_UPDATE_EMPLOYE);
            pstmt.setString(1, empl.getNom());
            pstmt.setString(2, empl.getPrenom());
            pstmt.setString(3, empl.getTelDom());
            pstmt.setString(4, empl.getTelPort());
            pstmt.setString(5, empl.getTelPro());
            pstmt.setString(6, empl.getAdresse());
            pstmt.setString(7, empl.getCodePostal());
            pstmt.setString(8, empl.getVille());
            pstmt.setString(9, empl.getEmail());
            pstmt.setInt(10, empl.getId());
            
            pstmt.executeUpdate();
            
        } catch (SQLException ex) {
            Logger.getLogger(ActionsBD.class.getName()).log(Level.SEVERE, null, ex);
        }finally{
            try {
                pstmt.close();
            } catch (SQLException ex) {
                Logger.getLogger(ActionsBD.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
    }

}

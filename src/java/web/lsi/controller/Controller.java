/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package web.lsi.controller;

import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import web.lsi.Utilisateur;
import web.lsi.model.ActionsBD;
import web.lsi.model.EmployeBean;
import web.lsi.utils.Constantes;

/**
 *
 * @author pierre
 */
@WebServlet(name = "Controller", urlPatterns = {"/Controller"})
public class Controller extends HttpServlet {
    EmployeBean employe = null;
    Utilisateur userInput;
    HttpSession session;
    private ActionsBD actionsBD;
    
    @Override
    public void init() throws ServletException {
        super.init();
        
        // 2. Creating an instance of ou EmployeDBUtil ... and pass in the connection pool / datasource
        try {
            this.actionsBD = new ActionsBD();
        }
        catch (Exception exc) {
            throw new ServletException(exc);
        }
    }


    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     * 
     * Here the method dispatch to other methods according to the action parameter
     * from the request.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = request.getParameter("action");
        //before doing any action : 
        //check if user is connected
        //else redirect him/her to login
        session = request.getSession();
        if(action.equals("login") || session.getAttribute("userBean") == null)
            login(request, response);
        else {
            switch(action) {
                case "list" :
                default :
                    listEmployees(request, response);
                    break;
                case "logout" :
                    logout(request, response);
                    break;
                case "details" :
                    details(request, response);
                    break;
                case "add" :
                    addEmployee(request, response);
                    break;
                case "add-panel" :
                    addEmployeePanel(request, response);
                    break;
                case "delete" :
                    deleteEmployee(request, response);
                    break;
                case "update" :
                    update(request, response);
            }
        }
         
    }
    
    /**
     * Get all the employees from the database and display them in a table
     * @param request
     * @param response 
     */
    private void listEmployees(HttpServletRequest request, HttpServletResponse response) {
        
        // 1. Get employes from DB Util
        List<EmployeBean> employes = actionsBD.getEmployees();
        if(employes.isEmpty()){
            request.setAttribute("NO_EMPLOYE_IN_BASE", Constantes.NO_EMPLOYE_IN_BASE);
        }
        
        // 2. Add employes to the request
        request.setAttribute("EMPLOYE_LIST", employes);
        
        try {
            // 3. Send to JSP page (view)
            request.getRequestDispatcher("/WEB-INF/list-employe.jsp").forward(request, response);
        } catch (ServletException | IOException ex) {
            Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
        }
    }


    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    /**
     * Check the fields in the login form and compare them with the database
     * Let the user through if his credentials are correct
     * 
     * @param request
     * @param response 
     */
    private void login(HttpServletRequest request, HttpServletResponse response) {
        if((request.getParameter("loginForm") == null) 
                || request.getParameter("mdpForm") == null
                || request.getParameter("loginForm").equals("") 
                || request.getParameter("mdpForm").equals("")
                ){
            
            //messErreur = "Vous devez renseigner les deux champs.\n";
            request.setAttribute("errKey", Constantes.LOGIN_AND_OR_MDP_EMPTY);
            
            try {
                request.getRequestDispatcher("/WEB-INF/index.jsp").forward(request, response);
            } catch (ServletException | IOException ex) {
                Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        else {
            
            session = request.getSession();

            userInput = new Utilisateur();
            userInput.setLogin(request.getParameter("loginForm"));
            userInput.setMdp(request.getParameter("mdpForm"));

            session.setAttribute("userBean", userInput);

            if (actionsBD.verifInfosConnexion(userInput)){
                listEmployees(request, response);
            }else{
                //messErreur = "Echec de la connexion ! Vérifiez votre login et/ou mot de passe et ressayez à nouveau.\n";
                request.setAttribute("errKey", Constantes.LOGIN_AND_OR_MDP_WRONG);
                try {
                    request.getRequestDispatcher("/WEB-INF/index.jsp").forward(request, response);
                } catch (ServletException | IOException ex) {
                    Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    /**
     * Get rid of the users credentials and return to the login form
     * @param request
     * @param response 
     */
    private void logout(HttpServletRequest request, HttpServletResponse response) {
        session = request.getSession();
        session.removeAttribute("userBean");
        try {
            request.getRequestDispatcher("/WEB-INF/index.jsp").forward(request, response);
        } catch (ServletException | IOException ex) {
            Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Display details of a specific employee
     * @param request
     * @param response 
     */
    private void details(HttpServletRequest request, HttpServletResponse response) {
        int theEmployeId;
        
        if(request.getParameter("selectedEmploye") == null || request.getParameter("selectedEmploye").equals("") ){
            request.setAttribute("NO_EMPLOYEE_SELECTED", Constantes.NO_EMPLOYEE_SELECTED_MODIF);
            listEmployees(request, response);
        }else{
            theEmployeId = Integer.parseInt(request.getParameter("selectedEmploye"));
            employe = actionsBD.getEmploye(theEmployeId);
            request.setAttribute("theEmploye", employe);
            try {
                request.getRequestDispatcher("/WEB-INF/displayOneEmployee.jsp").forward(request, response);
            } catch (ServletException | IOException ex) {
                Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    /**
     * Add a new employee to the db after filling a form
     * @param request
     * @param response 
     */
    private void addEmployee(HttpServletRequest request, HttpServletResponse response) {
        if((request.getParameter("nom") == null)
                || request.getParameter("nom").equals("")
                || request.getParameter("prenom") == null
                || request.getParameter("prenom").equals("")
                || request.getParameter("telDom") == null
                || request.getParameter("telDom").equals("")
                || request.getParameter("telPor") == null
                || request.getParameter("telPor").equals("")
                || request.getParameter("telPro") == null
                || request.getParameter("telPro").equals("")
                || request.getParameter("adresse") == null
                || request.getParameter("adresse").equals("")
                || request.getParameter("codePostal") == null
                || request.getParameter("codePostal").equals("")
                || request.getParameter("ville") == null
                || request.getParameter("ville").equals("")
                || request.getParameter("email") == null
                || request.getParameter("email").equals("")
                ){
            try {
                request.setAttribute("errAdd", Constantes.ADDITION_FAILED);
                request.getRequestDispatcher("/WEB-INF/addEmployeForm.jsp").forward(request, response);
                /*try{
                request.getRequestDispatcher("/WEB-INF/addEmployeForm.jsp").forward(request, response);
                }catch(ServletException | IOException ex){
                Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
                }*/
            } catch (ServletException | IOException ex) {
                Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
            }
        }else{
            
            //recup les infos saisies
            EmployeBean employeToAdd = new EmployeBean();
            employeToAdd.setNom(request.getParameter("nom"));
            employeToAdd.setPrenom(request.getParameter("prenom"));
            employeToAdd.setTelDom(request.getParameter("telDom"));
            employeToAdd.setTelPort(request.getParameter("telPor"));
            employeToAdd.setTelPro(request.getParameter("telPro"));
            employeToAdd.setAdresse(request.getParameter("adresse"));
            employeToAdd.setCodePostal(request.getParameter("codePostal"));
            employeToAdd.setVille(request.getParameter("ville"));
            employeToAdd.setEmail(request.getParameter("email"));
            
            //envoyer employeToAdd a actionBD 
            actionsBD.addEmploye(employeToAdd);
            if(actionsBD.getRowCount() != 1){
                try {
                    request.setAttribute("errAdd", Constantes.ADDITION_FAILED);
                    request.getRequestDispatcher("/WEB-INF/addEmployeForm.jsp").forward(request, response);
                } catch (ServletException | IOException ex) {
                    Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
                }
            }else{
                request.setAttribute("addSuccess", Constantes.ADDITION_SUCCESS);
                listEmployees(request, response);
            }
        }
        
        
    }

    /**
     * Delete an employee from the DB after selecting its radio button and 
     * clicking on delete 
     * @param request
     * @param response 
     */
    private void deleteEmployee(HttpServletRequest request, HttpServletResponse response) {
        int theEmployeId;

        if(request.getParameter("selectedEmploye") == null || request.getParameter("selectedEmploye").equals("") ){
            request.setAttribute("NO_EMPLOYEE_SELECTED", Constantes.NO_EMPLOYEE_SELECTED);
            listEmployees(request, response);
        }else{
            // 1. Read student id from form
            theEmployeId = Integer.parseInt(request.getParameter("selectedEmploye"));

            // 2. Delete employe from database
            actionsBD.deleteEmploye(theEmployeId);

            if(actionsBD.getRowCount() == 1){

                request.setAttribute("deleteSuccess", Constantes.DELETE_SUCCESSFUL);

                // 3. Send them back to the list student page
                listEmployees(request, response);

            }
        } 

    }
    
    /**
     * Update fields of a specific employee in the database after changing infos
     * regarding this employee
     * @param request
     * @param response 
     */
    private void update(HttpServletRequest request, HttpServletResponse response){
        
        int theEmployeId = Integer.parseInt(request.getParameter("selectedEmploye"));

        employe.setId(theEmployeId);
        employe.setNom(request.getParameter("nom"));
        employe.setPrenom(request.getParameter("prenom"));
        employe.setTelDom(request.getParameter("telDom"));
        employe.setTelPort(request.getParameter("telPor"));
        employe.setTelPro(request.getParameter("telPro"));
        employe.setAdresse(request.getParameter("adresse"));
        employe.setCodePostal(request.getParameter("codePostal"));
        employe.setVille(request.getParameter("ville"));
        employe.setEmail(request.getParameter("email"));
        actionsBD.update(employe);
        
        listEmployees(request, response);
    }

    /**
     * Display the add employee form so that the user can fill in a new employee
     * @param request
     * @param response 
     */
    private void addEmployeePanel(HttpServletRequest request, HttpServletResponse response) {
        try {
            request.getRequestDispatcher("/WEB-INF/addEmployeForm.jsp").forward(request, response);
        } catch (ServletException | IOException ex) {
            Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}

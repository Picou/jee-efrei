# Projet JAVA EE 2019 - 2020

## Objectif du Projet

Concevoir et réaliser une application web de gestion d'employés en Java EE

## Processus de Réalisation

Le projet a été conçu en respectant les consignes données. Celui-ci à été conçu et implémenté par 

1. Equipe de développement
    -`Pierre Couderc`
    -`Philippe Zhang`
    -`Ulrich Ekeu Monkam`
    
Le projet à été développé sous 3 versions comme requis par les spécifications. Et les différentes version sont réparties sur 3 branches comme suit:

-`La branche master` qui fait reférence de version 1
-`La branche v2_ulrich` qui fait reférence de version 2
-`La branche v3_ulrich` qui fait reférence de version 3

## Etat de développement 

1. Version 1
    - [x] JSP (scriptlets acceptés) + Java Beans + Servlet + JDBC
    - [x] Script SQL dans WEB-INF
    - [x] Tous les fichiers JSP dans WEB-INF
    - [x] SGBD : Java DB
    
2. Version 2
    - [x] Projet Maven
    - [x] JSP (EL et JSTL uniquement / zéro code Java dans vos fichiers JSP !) + Java Beans
    - [x] Servlet (optimisée)
    - [x] Persistance avec JPA
    - [x] SGBD : MySQL

2. Version 3
    - [x] L’usage d’annotations dans votre servlet pour remplacer tout ou une partie du web.xml 
    - [x] L’utilisation de Web services RESTful 
    - [] Le déploiement sur le Cloud


## Comment Exécuter le Projet

### Version_1 
### Version_2

-   Téléchargez l'une des versions 
-   Ouvrez-le dans votre IDE de choix (Netbeans de preférence)
-   Exécutez le fichier `.sql` qui se trouve dans le dossier `WEB-INF` pour créer la base de données
-   Définissez le `nom d'utilisateur` et `mot de passe` comme `jee` et `jee` respectivement (pour la base de données Derby)
-   Définissez le `nom d'utilisateur` et `mot de passe` comme `jee` et `jeeprj` respectivement (pour la base de données MYSQL)


### Version_3

- Téléchargez la version 3
- Ouvrez dans votre IDE de choix (Netbeans de preférence)
- Faites un clique droit sur le dossier `Restful Web Services` et choisissez `Test Restful Web Services`

>Si cela ne fonctionne pas (cas le plus probable) Ouvrez un outil de test d'api REST tel que `postman` et entrez-y vos différentes requêtes.

Par exemple:

>`GET`: `http://localhost:8080/jee-efrei_v3/webresources/employe/3`


## Difficultés rencontrés

- En soi, durant tout le procesus de développement, nous n'avions pas eu de difficultés majeures rencontrés jusqu'a la version 3 où il fallait déployer.
- Durant cette version, quand il fallait migrer du serveur `Glassfish` vers `Tomcat` nous avons rencontré l'erreur ci-dessous:

>`22-Nov-2019 13:33:13.388 SEVERE [http-nio-8080-exec-11] org.apache.catalina.core.StandardWrapperValve.invoke Servlet.service() for servlet [JEEServlet] in context with path [/jee-efrei_v3] threw exception
    java.lang.NullPointerException`
    
-Difficile à corriger, nous nous sommes tournés vers `StackOverflow` ou nous avons posé notre question mais malgré cela nous n'avons pas eu de réponse concernant notre problématique. Ainsi, difficile pour nous de déployer

>Lien vers la question: [Problème posé](https://stackoverflow.com/questions/58997183/nullpointerexception-when-setting-attribute-in-tomcat/58997779#58997779)